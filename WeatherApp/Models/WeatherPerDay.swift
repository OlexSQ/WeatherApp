//
//  WeatherPerDay.swift
//  WeatherApp
//
//  Created by Mac on 28.04.2021.
//

import Foundation
import UIKit


struct WeatherPerDay {
    let weatherImageView = UIImageView()
    let date: String
    let temp: Int
    let desc: String
}
