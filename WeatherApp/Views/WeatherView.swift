//
//  WeatherView.swift
//  WeatherApp
//
//  Created by Mac on 26.04.2021.
//

import UIKit

class WeatherView: UIView {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var degreesLabel: UILabel!
    

}

